describe('Los estudiantes login', function() {
    it('Visits los estudiantes and fails at login', function() {
      cy.visit('https://losestudiantes.co')
      cy.contains('Cerrar').click()
      //Lineas nuevas  
      cy.contains('Ingresar').click()
      /*
      	1. Crear una cuenta con correo ya existente
      */
      cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Prueba")
      cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Cypress")
      cy.get('.cajaSignUp').find('input[name="correo"]').click().type("correo@prueba.com")
      cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Administración')
      cy.get('.cajaSignUp').find('input[name="password"]').click().type("12345678")
      cy.get('.cajaSignUp').find('input[name="acepta"]').click()
      cy.contains('Registrarse').click()

      cy.contains('Ok').click()

      /*
		Se cambia el correo a uno nuevo para registro correcto
		NOTA: toca cambiar el número en el correo o hacer cambios por cada intento
      */
      cy.get('.cajaSignUp').find('input[name="correo"]').click().clear()
      cy.get('.cajaSignUp').find('input[name="correo"]').click().type('correodepruebasatuomaticas2121@prueba2121.com')
      cy.get('.cajaSignUp').find('button[type="submit"]').click()
      cy.wait(3000)
      cy.contains('Ok').click()

      /*
		2. Buscar a un profesor
      */
      cy.get('form').find('span[class="Select-arrow"]').click()
      cy.get('form').find('input').type('Mario Linares')

      /*
		3. Entrar a la página de algún profesor
      */
      cy.wait(2000)
      cy.get('form').find('div[class="Select-menu-outer"]').click()

      /*
		4. Filtors por materia para un profesor
      */
      cy.get('.materias').find('input[name="id:ISIS3510"]').click()
      cy.get('.materias').find('input[name="id:ISIS1206"]').click()
      cy.get('.materias').find('input[name="id:ISIS3710"]').click()
      cy.get('.materias').find('input[name="id:ISIS3510"]').click()
      cy.get('.materias').find('input[name="id:ISIS1206"]').click()
      cy.get('.materias').find('input[name="id:ISIS3710"]').click()
    })
})